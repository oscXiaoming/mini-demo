const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return `${[year, month, day].map(formatNumber).join('/')} ${[hour, minute, second].map(formatNumber).join(':')}`
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : `0${n}`
}

const httpGet = (url, data) => {
  wx.request({
    url: url,
    method: 'GET',
    data: data
  })
}

const httpPost = (url, data) => {
  wx.request({
    url: url,
    method: 'POST',
    header: {"Content-Type": "application/x-www-form-urlencoded"},
    data: data
  })
}

module.exports = {
  formatTime,
  httpGet,
  httpPost
}

Page({

    /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    // 用户登录
    wx.login({
      success(res) {
        if (res.code) {
          wx.request({
            url: 'http://localhost:8080/api/getUserInfo',
            data: {
              jsCode: res.code
            },
            success(res) {
              console.log(res.data)
              wx.setStorage({
                data: res.data.data.loginStatus,
                key: 'loginStatus',
              })
            }
          })
        } else {
          console.log(`登录失败` + res.errMsg)
        }
      }
    })
  }

})

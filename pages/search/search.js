// pages/index/search.js
Page({

  data: {
    status: ''
  },
  onReady: function() {
    wx.getStorage({
      key: 'loginStatus',
      success: (res) => {
        this.setData({
          status: res.data
        })
        
      },
      error: function(res) {
        console.log(res);
      }
    })
  }
})
